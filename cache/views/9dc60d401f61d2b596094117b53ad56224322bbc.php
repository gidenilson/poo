<?php $__env->startSection('content'); ?>
    <div class="container">

        <h1>Clientes</h1>
        <table class="table">
            <thead>
            <th><a <?php if($order == 'up'): ?> href="down" <?php else: ?> href="up" <?php endif; ?>>
                    <?php if($order == 'up'): ?> <i class="glyphicon glyphicon-triangle-top"></i>
                    <?php else: ?> <i class="glyphicon glyphicon-triangle-bottom"></i>
                    <?php endif; ?>
                </a></th>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Número</th>
            <th></th>
            </thead>
            <tbody>
            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $client): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                <tr>

                    <td><?php echo e($client->getId()); ?></td>
                    <td><a class="link-name" href='#' data-href="/client/<?php echo e($client->getId()); ?>"><?php echo e($client->getName()); ?></a></td>
                    <td><?php echo e(strtoupper($client->getDoc()->getType())); ?></td>
                    <td><?php echo e($client->getDoc()->getNumber()); ?></td>
                    <td>
                        <?php for($i = 1; $i<=5 ; $i ++): ?>
                        <?php if($i <= $client->getValue()): ?> <i class="glyphicon glyphicon-star"/> <?php endif; ?>
                        <?php if($i > $client->getValue()): ?> <i class="glyphicon glyphicon-star-empty"/> <?php endif; ?>
                        <?php endfor; ?>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>


            </tbody>
        </table>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cliente</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>