<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 25/09/2016
 * Time: 11:46
 */

namespace App\Collections;


use App\Classes\Client;
use App\Classes\Doc;
use App\Interfaces\DataCollectionInterface;

class Clients implements DataCollectionInterface
{


    public $clients;
    public $data;

    /**
     * Clients constructor.
     * @param $data
     */
    public function __construct(array $data)
    {

        $this->setData($data);
        $this->data = $data;
        return $this->clients;
    }

    private function setData(array $data)
    {
        $clients = [];
        $id = 0;
        foreach ($data as $item) {
            $client = new Client();
            $doc = new Doc($item['type'], $item['number']);


            $clients[] = $client->setDoc($doc)
                ->setAddress2($item['address2'])
                ->setName($item['name'])
                ->setGender($item['gender'])
                ->setAddress($item['address'])
                ->setValue($item['value'])
                ->setId($id++);
        }
        $this->clients = $clients;

    }


    public function find($id)
    {
        $item = $this->data[$id];
        $client = new Client();
        $doc = new Doc($item['type'], $item['number']);
        $client->setDoc($doc)
            ->setAddress2($item['address2'])
            ->setName($item['name'])
            ->setValue($item['value'])
            ->setGender($item['gender'])
            ->setAddress($item['address']);
        return $client;
    }

    public function getAll($asc = true)
    {
        $array = $this->clients;
        if (!$asc) {
            $array = array_reverse($this->clients);
        }
        return $array;
    }
}