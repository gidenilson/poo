<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 07/10/2016
 * Time: 22:42
 */

namespace App\Interfaces;


interface CollectableAddressInterface
{
    public function hasAddress2();
    public function setAddress2($address);
    public function getAddress2();
    public function removeAddress2();

}