<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 25/09/2016
 * Time: 11:38
 */

namespace App\Interfaces;


interface DataCollectionInterface
{
    public function __construct(array $data);

    public function getAll($asc);

    public function find($id);
}