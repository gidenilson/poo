<?php

namespace App\Interfaces;

interface EvaluableInterface{
    public function setValue($value);
    public function getValue();
    public function incValue();
    public function decValue();

}