<?php

namespace App\Data;
class Clients
{
    public static function data($asc = true)
    {
        $data = [

            [
                'id' => 1,
                'name' => 'Gustavo Arthur Miguel Martins',
                'gender' => 'M',
                'type' => 'cpf',
                'value' => 2,
                'number' => '17710717020',
                'address' => 'Rua Altair Vicente Sagaz, 762',
                'address2' => 'Rua Itamaraty, 87'
            ],
            [
                'id' => 2,
                'name' => 'Benício Diogo Leonardo Moura',
                'gender' => 'M',
                'type' => 'cpf',
                'value' => 5,
                'number' => '29134137572',
                'address' => 'Rua Euclides João dos Santos, 988',
                'address2' => ''
            ],
            [
                'id' => 3,
                'name' => 'Mariah Catarina Julia Barros',
                'gender' => 'F',
                'type' => 'cnpj',
                'value' => 3,
                'number' => '94868536060',
                'address' => 'Alameda das Orquídeas, 777',
                'address2' => ''
            ],
            [
                'id' => 4,
                'name' => 'Luna Hadassa Cardoso',
                'gender' => 'F',
                'type' => 'cpf',
                'value' => 3,
                'number' => '17320857570',
                'address' => 'Rua Doutor Nilton Velmosvitsky, 124',
                'address2' => 'Rua Bosco da Silva, 211'
            ],
            [
                'id' => 5,
                'name' => 'Heitor Augusto Nascimento',
                'gender' => 'M',
                'type' => 'cnpj',
                'value' => 1,
                'number' => '85087940889',
                'address' => 'Avenida Nossa Senhora dos Remédios, 453',
                'address2' => ''
            ],
            [
                'id' => 6,
                'name' => 'Agatha Mirella Alícia Dias',
                'gender' => 'F',
                'type' => 'cnpj',
                'value' => 4,
                'number' => '99325074907',
                'address' => 'Rua Raimundo Nonato, 901',
                'address2' => 'Av Jorônimo Carvalho, 453'
            ],
            [
                'id' => 7,
                'name' => 'Pietra Bianca Milena dos Santos',
                'gender' => 'F',
                'type' => 'cpf',
                'value' => 3,
                'number' => '32569985694',
                'address' => 'Rua José Valci Bezerra Cavalcante, 2567',
                'address2' => ''
            ],
            [
                'id' => 8,
                'name' => 'Sara Isabel Freitas',
                'gender' => 'F',
                'type' => 'cpf',
                'value' => 2,
                'number' => '17956024145',
                'address' => 'Rua Rosauro Mariano da Silva, 1675',
                'address2' => 'Rua Carlos Drumond, 901'
            ],
            [
                'id' => 9,
                'name' => 'João Caio Carvalho',
                'gender' => 'M',
                'type' => 'cnpj',
                'value' => 3,
                'number' => '47980375831',
                'address' => 'Rua Marte, 365',
                'address2' => 'Av Juca Chaves, 81'
            ],
            [
                'id' => 10,
                'name' => 'Gabriel Joaquim Monteiro',
                'gender' => 'M',
                'type' => 'cnpj',
                'value' => 1,
                'number' => '90703291742',
                'address' => 'Praça Benjamin Reginato, 230',
                'address2' => ''
            ],

        ];
        return $data;
    }

}