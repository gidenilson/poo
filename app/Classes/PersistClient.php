<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 17/10/2016
 * Time: 22:17
 */

namespace App\Classes;




class PersistClient
{
    private $pdo;
    private $sql;
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function persist(Client $client)
    {
       $this->client = $client;
       $this->sql = "INSERT INTO clients (name, gender, type, number, address, address2, value)"
              . "values("
              . "'{$client->getName()}', "
              . "'{$client->getGender()}', "
              . "'{$client->getDoc()->getType()}', "
              . "'{$client->getDoc()->getNumber()}', "
              . "'{$client->getAddress()}', "
              . "'{$client->getAddress2()}', "
              . "'{$client->getValue()}' "
              . ")";

    }

    public function flush()
    {

        $this->pdo->exec($this->sql);
    }
}



