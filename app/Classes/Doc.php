<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 07/10/2016
 * Time: 17:32
 */

namespace App\Classes;


class Doc
{
    private $type;
    private $number;

    /**
     * Doc constructor.
     * @param $type
     * @param $number
     */
    public function __construct($type = null, $number = null)
    {
        $this->type = $type;
        $this->number = $number;
    }


    public function getType()
    {
        return $this->type;
    }


    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }


    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }


}