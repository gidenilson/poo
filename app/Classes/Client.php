<?php
/**
 * Created by PhpStorm.
 * User: Gidenilson
 * Date: 07/10/2016
 * Time: 17:27
 */

namespace App\Classes;


use App\Abstracts\AClient;
use App\Interfaces\CollectableAddressInterface;
use App\Interfaces\EvaluableInterface;

class Client extends AClient implements EvaluableInterface, CollectableAddressInterface
{
    private $value;
    private $address2;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        if ($value > 5) {
            $value = 5;
        }
        $this->value = $value;
        return $this;
    }

    public function incValue()
    {
        $this->value++;
        if ($this->value > 5) {
            $this->value = 5;
        }
        return $this;
    }

    public function decValue()
    {
        $this->value--;
        if ($this->value < 1) {
            $this->value = 1;
        }
        return $this;
    }


    public function hasAddress2()
    {
        return $this->address2 == true;
    }

    public function setAddress2($address)
    {
        $this->address2 = $address;
        return $this;
    }

    public function getAddress2()
    {
        return $this->address2;
    }

    public function removeAddress2()
    {
        $this->address2 = "";
        return $this;
    }
}