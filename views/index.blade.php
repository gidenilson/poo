@extends('layouts.layout')
@section('content')
    <div class="container">

        <h1>Clientes</h1>
        <table class="table">
            <thead>
            <th><a @if($order == 'up') href="down" @else href="up" @endif>
                    @if($order == 'up') <i class="glyphicon glyphicon-triangle-top"></i>
                    @else <i class="glyphicon glyphicon-triangle-bottom"></i>
                    @endif
                </a></th>
            <th>Nome</th>
            <th>Tipo</th>
            <th>Número</th>
            <th></th>
            </thead>
            <tbody>
            @foreach($clients as $client)
                <tr>

                    <td>{{$client->getId()}}</td>
                    <td><a class="link-name" href='#' data-href="/client/{{ $client->getId() }}">{{$client->getName()}}</a></td>
                    <td>{{ strtoupper($client->getDoc()->getType()) }}</td>
                    <td>{{$client->getDoc()->getNumber()}}</td>
                    <td>
                        @for($i = 1; $i<=5 ; $i ++)
                        @if($i <= $client->getValue()) <i class="glyphicon glyphicon-star"/> @endif
                        @if($i > $client->getValue()) <i class="glyphicon glyphicon-star-empty"/> @endif
                        @endfor
                    </td>
                </tr>
            @endforeach


            </tbody>
        </table>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cliente</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body&hellip;</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
