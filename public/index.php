<?php



use duncan3dc\Laravel\Blade;
use Klein\Klein;


require "../bootstrap.php";
require "database.php";

/*
 * Roteamento
 */
$klein = new Klein();



$klein->respond('GET', '/', function ($request) use ($clients) {
    return Blade::render("index", [
        'clients' => $clients->getAll($request->order == 'up'),
        'order' => $request->order
    ]);
});

$klein->respond('GET', '/[:order]', function ($request) use ($clients) {
     return Blade::render("index", [
        'clients' => $clients->getAll($request->order == 'up'),
        'order' => $request->order
    ]);
});

$klein->respond('GET', '/client/[:id]', function ($request) use ($clients) {
    return Blade::render("client", [
        'client' => $clients->find($request->id)
    ]);
});

$klein->dispatch();