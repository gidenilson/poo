<?php
use App\Classes\Doc;
use App\Classes\PersistClient;
use App\Classes\Client;
use App\Collections\Clients;

try {
    /*
     * Base de dados SQLITE
     */
    $pdo = new \PDO("sqlite:..\database\database.sqlite");

} catch (\PDOException $e) {
    die($e->getMessage());
}


/*
 * cria a tabela no banco de dados
 */
$sql = "DROP  TABLE IF EXISTS clients;"
    . "CREATE TABLE clients ("
    . "id       INTEGER PRIMARY KEY AUTOINCREMENT,"
    . "name     VARCHAR (60),"
    . "gender   VARCHAR (3),"
    . "type     VARCHAR (10),"
    . "number   VARCHAR (20),"
    . "address  VARCHAR (255),"
    . "address2 VARCHAR (255),"
    . "value    INTEGER)";
$pdo->exec($sql);

/*
 * Popula tabela com ajuda de PersistClient
 */
$persistClient = new PersistClient($pdo);

$client1 = new Client();
$client1->setDoc(new Doc('cnpj', '7373636383838'))
    ->setName("João da Silva")
    ->setAddress("Rua Mantiqueira, 4334")
    ->setGender("M")
    ->setValue(3);

$client2 = new Client();
$client2->setDoc(new Doc('cpf', '87667654398'))
    ->setName("Maria Flávia de Almeida")
    ->setAddress("Av. 13 de maio, 102")
    ->setAddress2("Praça Colombo, 134 - apto 58")
    ->setGender("F")
    ->setValue(4);

$client3 = new Client();
$client3->setDoc(new Doc('cnpj', '26552444255'))
    ->setName("Graça Maria de Castro")
    ->setAddress("Av. Marginal, 1142")
    ->setGender("F")
    ->setValue(5);


//persiste clientes na base de dados
$persistClient->persist($client1);
$persistClient->flush();

$persistClient->persist($client2);
$persistClient->flush();

$persistClient->persist($client3);
$persistClient->flush();

/*
 * Cria collection a partir da base de dados
 */

$result = $pdo->query("select * from clients");
$result = $result->fetchAll(PDO::FETCH_ASSOC);
$clients = new Clients($result);
